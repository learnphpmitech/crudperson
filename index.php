<?php
require_once("Class/DbController.php");
require_once("Class/Person.php");

$db_handle = new DbController();

if (!empty($_GET["action"])) {
    $action = $_GET["action"];
}
switch ($action) {
    case "person-add":
        if (isset($_POST['add'])) {
            $fullName = $_POST["fullname"];
            $gender = $_POST["gender"];
            $phone = $_POST["phone"];
            $email = $_POST["email"];
            $person = new Person();
            $insertId = $person->addPerSon($fullName, $gender, $phone, $email);
            if (empty($insertId)) {
                $response = array(
                    "message" => "Problem in adding new person",
                    "type" => "error"
                );
            } else {
                header("Location: index.php");
            }
        }
        require_once "Web/person-add.php";
        break;
    case "person-edit":
        $person_id = $_GET["id"];
        $person = new Person();
        if(isset($_POST["edit"])){
            $fullName = $_POST["fullname"];
            $gender = $_POST["gender"];
            $phone = $_POST["phone"];
            $email = $_POST["email"];
            $person->editPerson($person_id, $fullName, $gender, $phone, $email);
            header("Location: index.php");
        }
        $result = $person->getPersonById($person_id);
        require_once "Web/person-edit.php";
        break;
    case "person-delete":
        $person_id = $_GET["id"];
        $person = new Person();
        $person->deletePerson($person_id);
        $result = $person->getAllPerson();
        require_once "Web/person.php";
        break;
    case "person-search":
        $search = $_POST["search"];
        $person = new Person();
        $result = $person->searchPersonByName($search);
        require_once "Web/person.php";
        break;
    default:
        $person = new Person();
        $result = $person->getAllPerson();
        require_once "Web/person.php";
        break;
        
    
}
