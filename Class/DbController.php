<?php
header("Content-type: text/html; charset=utf-8");
include('./Config/config.php');
class DbController {
    
    private $host = DB_HOST;
    private $user = DB_USER;
    private $password = DB_PASSWORD;
    private $database = DB_DATABASE;
    private $conn;
    function __construct()
    {
        $this->conn =  $this->connectDb();
    }
    function connectDb(){
        $conn = mysqli_connect($this->host, $this->user, $this->password, $this->database);
        mysqli_set_charset($conn, 'UTF8');
        return $conn;
    }
    function runBaseQuery($query) {
        $resultQuery = $this->conn->query($query);
        if($resultQuery->num_rows > 0) {
            while($row = $resultQuery->fetch_assoc()){
                $resultset[] = $row;
            }
        }
        return $resultset;
    }

    function runQuery($query, $param_type, $param_value_array) {
        $sql = $this->conn->prepare($query);
        $this->bindQueryParams($sql, $param_type, $param_value_array);
        $sql->execute();
        $sql->bind_result($id, $fullname, $gender, $phone, $email);

        while ($sql->fetch()) {
            $person = array();
            $person["Id"] = $id;
            $person["FullName"] = $fullname;
            $person["Gender"] = $gender;
            $person["Phone"] = $phone;
            $person["Email"] = $email;
        }

        return $person;
    }
    function bindQueryParams($sql, $param_type, $param_value_array) {
        $param_value_reference[] = & $param_type;
        for($i = 0; $i<count($param_value_array); $i++){
            $param_value_reference[] = & $param_value_array[$i];
        }
        call_user_func_array(array(
            $sql,
            'bind_param'
        ), $param_value_reference);
    }
    function insert($query, $param_type, $param_value_array) {
        $sql = $this->conn->prepare($query);
        $this->bindQueryParams($sql, $param_type, $param_value_array);
        $sql->execute();
        $insertId = $sql->insert_id;
        return $insertId;
    }
    function update($query, $param_type, $param_value_array){
        $sql = $this->conn->prepare($query);
        $this->bindQueryParams($sql, $param_type, $param_value_array);
        $sql->execute();
    }
}
?>