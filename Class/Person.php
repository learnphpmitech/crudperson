<?php 
require_once("Class/DbController.php");
class Person{
    private $db_handle;
    function __construct(){
        $this->db_handle =  new DbController();
    }
    function addPerSon($fullName, $gender, $phone, $email){
        $query = "INSERT INTO Persons (FullName, Gender, Phone, Email) VALUES (?,?,?,?)";
        $paramType = "siss";
        $paramValue = array(
            $fullName,
            $gender,
            $phone,
            $email
        );
        $insertId = $this->db_handle->insert($query, $paramType, $paramValue);
        return $insertId;
    }
    function editPerson($id, $fullName, $gender, $phone, $email){
        $query = "UPDATE Persons SET FullName = ?, Gender = ?, Phone = ?, Email = ? Where Id = ? ";

        $paramType = "sissi";
        $paramValue = array(
            $fullName,
            $gender,
            $phone,
            $email,
            $id
        );
        $this->db_handle->update($query, $paramType, $paramValue);
    }
    function deletePerson($person_id){
        $query = "DELETE FROM Persons WHERE Id = ?";
        $paramType = "i";
        $paramValue = array(
            $person_id
        );
        $this->db_handle->update($query, $paramType, $paramValue);
    }
    function getPersonById($person_id){
        
        $query = "SELECT Id, FullName, Gender, Phone, Email FROM Persons WHERE Id = ?";
        $paramType = "i";
        $paramValue = array(
            $person_id
        );
        
        $result = $this->db_handle->runQuery($query, $paramType, $paramValue);
        
        return $result;
    }

    function getAllPerson(){
        $sql = "SELECT * FROM Persons ORDER BY FullName ASC";
        $result = $this->db_handle->runBaseQuery($sql);
        return $result;
    }

    function searchPersonByName($search){
        $sql = "SELECT * FROM Persons Where FullName like '%$search%' ";
        //die(var_dump($search));
        $result = $this->db_handle->runBaseQuery($sql);
        return $result;
    }
}