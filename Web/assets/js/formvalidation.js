$(document).ready(function() {
 
    $("#form-add-person").validate({
        rules: {
            fullname: "required",
            gender: "required",
            phone: {
                required: true,
                maxlength: 10,
                number   : true,
            },
            email: {
                required: true,
                email: true
            },
        },
        messages: {
            fullname: "Please type your name",
            gender: "You forgot your gender?",
            phone: {
                required: "Add your phone so i can find you",
                maxlength: "in Vn phone number has 10 num"
            }
        }
    });
    $("#form-edit-person").validate({
        rules: {
            fullname: "required",
            gender: "required",
            phone: {
                required: true,
                maxlength: 10,
                number   : true,
            },
            email: {
                required: true,
                email: true
            },
        },
        messages: {
            fullname: "Please type your name",
            gender: "You forgot your gender?",
            phone: {
                required: "Add your phone so i can find you",
                maxlength: "in Vn phone number has 10 num"
            }
        }
    });
});
