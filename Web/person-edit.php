<?php require_once "Web/layout/header.php" ?>
<div class="container">
    <form action="index.php?action=person-edit&id=<?php echo $result['Id'] ?>" method="POST" role="form">
        <legend>Edit Person</legend>
        <div class="form-group">
            <label for="">Full Name</label>
            <input type="text" class="form-control" id="" placeholder="Type your name here" value="<?php echo $result['FullName'] ?>" name="fullname">
        </div>
        <div class="form-group">
            <label for="">Gender</label>
            <select name="gender" id="input" class="form-control" required="required">
                
                <option value="" disabled>Choose your gender</option>
                <option value=0 <?php if($result['Gender'] == 0) echo 'selected' ?>>Nữ</option>
                <option value=1 <?php if($result['Gender'] == 1) echo 'selected' ?>>Nam</option>
            </select>

        </div>
        <div class="form-group">
            <label for="">Phone Number</label>
            <input type="text" class="form-control" id="" value="<?php echo $result['Phone'] ?>" placeholder="Type your phone number here" name="phone">
        </div>
        <div class="form-group">
            <label for="">Email Address</label>
            <input type="text" class="form-control" id="" value="<?php echo $result['Email'] ?>"  placeholder="Type your email here" name="email">
        </div>
        <button type="submit" class="btn btn-primary" name="edit" id="btnSubmit" value="Save">Submit</button>
    </form>
</div>
<?php require_once "Web/layout/footer.php" ?>