<?php require_once "Web/layout/header.php" ?>
<div class="container">
    <div class="table-wrapper">
        <div class="table-title">
            <div class="row">
                <div class="col-sm-12">
                    <h2>Manage <b>Persons</b></h2>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-6">
                    <form class="example" action="index.php?action=person-search" method="POST">
                        <input type="text" placeholder="Search.." name="search">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <div class="col-sm-6">
                    <a href="index.php?action=person-add" class="btn btn-success"><i class="material-icons">&#xE147;</i> <span>Add New Person</span></a>
                </div>
            </div>
        </div>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Full Name</th>
                    <th>Gender</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (!empty($result)) {
                    foreach ($result as $key => $value) {

                        ?>
                        <tr>
                            <td><?php echo $value['FullName']; ?></td>
                            <td><?php if ($value['Gender']) echo "Nam";
                                        else echo "Nữ"; ?></td>
                            <td><?php echo $value['Phone'] ?></td>
                            <td><?php echo $value['Email'] ?></td>
                            <td>
                                <a href="index.php?action=person-edit&id=<?php echo $value["Id"]; ?>" class="edit"><i class="material-icons" title="Edit">&#xE254;</i></a>
                                <a href="index.php?action=person-delete&id=<?php echo $value["Id"]; ?>"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                            </td>
                        </tr>
                <?php
                    }
                }
                ?>
            </tbody>
        </table>
        <!-- <div class="clearfix">
        <div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>
        <ul class="pagination">
            <li class="page-item disabled"><a href="#">Previous</a></li>
            <li class="page-item"><a href="#" class="page-link">1</a></li>
            <li class="page-item"><a href="#" class="page-link">2</a></li>
            <li class="page-item active"><a href="#" class="page-link">3</a></li>
            <li class="page-item"><a href="#" class="page-link">4</a></li>
            <li class="page-item"><a href="#" class="page-link">5</a></li>
            <li class="page-item"><a href="#" class="page-link">Next</a></li>
        </ul>
    </div> -->
    </div>
</div>

<?php require_once "Web/layout/footer.php" ?>