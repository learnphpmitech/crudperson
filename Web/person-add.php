<?php require_once "Web/layout/header.php"?>
<div class="container">
<form action="index.php?action=person-add" method="POST" id="form-add-person" role="form">
    <legend>Add Person</legend>
    <div class="form-group">
        <label for="">Full Name</label>
        <input type="text" class="form-control" id="" placeholder="Input field" name="fullname">
    </div>
    <div class="form-group">
        <label for="">Gender</label>
        <select name="gender" id="input" class="form-control" required="required">
            <option value="" selected disabled>Choose your gender</option>
            <option value=0>Nữ</option>
            <option value=1>Nam</option>
        </select>
        
    </div>
    <div class="form-group">
        <label for="">Phone Number</label>
        <input type="text" class="form-control" id="" placeholder="Input field" name="phone">
    </div>
    <div class="form-group">
        <label for="">Email Address</label>
        <input type="text" class="form-control" id="" placeholder="Input field" name="email">
    </div>
    <button type="submit" class="btn btn-primary" name="add" id="btnSubmit" value="Save">Submit</button>
</form>
</div>
<script src="Web/assets/js/formvalidation.js"></script>
<?php require_once "Web/layout/footer.php" ?>